﻿using System;
using System.Text.RegularExpressions;

namespace EmailChecker
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Write a text with emails included!");

            string text = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(text))
            {
                Console.WriteLine("No blanks spaces. Text with Emails included");
                text = Console.ReadLine();
            }

            MatchCollection emailMatch = check4Email(text);
            int numberOfEmails = emailMatch.Count;

            if (numberOfEmails > 0)
            {
                Console.WriteLine($"A total of {numberOfEmails} emails, were found.");
                foreach (var email in emailMatch)
                {
                    Console.WriteLine(email);
                }
            }
            else
            {
                Console.WriteLine($"No emails were found.");
            }

        }


        static MatchCollection check4Email(string text)
        {
            string emailPattern = @"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+";

            Regex regex = new Regex(emailPattern, RegexOptions.IgnoreCase);

            MatchCollection emailMatch = regex.Matches(text);
            return emailMatch;
        }

    }
}
